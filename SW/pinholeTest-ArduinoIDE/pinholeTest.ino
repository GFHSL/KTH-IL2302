// These constants won't change.  They're used to give names
// to the pins used:
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to

int sensorValueA = 0;        // value read from the pot
int sensorValueB = 0;        // value read from the pot
int valuesA[10],valuesB[10];
int i,j;
int sumA,sumB = 0;
float smoothedVal;
float filterVal = 0.7;
int digitalPin = 13;

void setup() {
  for(i = 9;i>=0;i--){
    valuesA[i] = 0;
    valuesB[i] = 0;
  }
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  pinMode(digitalPin,OUTPUT);
  delay(100);
  Serial.println("");
}


int sensorValue;
void loop() {
  i%=10;
  sumA = 0;
  sumB = 0;
  // read the analog in value:
  sensorValueA = analogRead(analogInPin);
  digitalWrite(digitalPin,HIGH); //big
  delay(10);
  sensorValueB = analogRead(analogInPin);
  digitalWrite(digitalPin, LOW); //small
  // print the results to the serial monitor:
  //Serial.print("sensor = ");
//  Serial.println(sensorValue);
  valuesA[i] = sensorValueA;
  valuesB[i] = sensorValueB;
  for(j = 0;j<10;j++){
    sumA += valuesA[j];
    sumB += valuesB[j];
  }
//  Serial.println(sum/10);

//  Serial.println(sumA/10);
//  Serial.print(" ");
//  Serial.println(sumB/10);
  smoothedVal = smooth(sumB/10,filterVal,smoothedVal);
  Serial.println(smoothedVal);
  // wait 2 milliseconds before the next loop
  // for the analog-to-digital3 converter to settle
  // after the last reading:
  delay(10);
  i++;
}

int smooth(int data, float filterVal, float smoothedVal){


  if (filterVal > 1){      // check to make sure param's are within range
    filterVal = .99;
  }
  else if (filterVal <= 0){
    filterVal = 0;
  }

  smoothedVal = (data * (1 - filterVal)) + (smoothedVal  *  filterVal);

  return (int)smoothedVal;
}

