void digital_write(volatile uint8_t *data_port, int pin, int value);
int digital_read(int input_register, int pin);
void digital_init(volatile uint8_t *data_port, int pin, int mode);
void adc_init(void);
uint16_t adc_read(uint8_t adcx);
