#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>

#include "main.h"
#include "uart.h"
#include "gpio.h"

#define BLINK_DELAY_MS 500

int main(void)
{
        digital_init(&DDRB,DDB5,1);
        uart_init();

        while(1)
        {
                puts("Hello world!");
                digital_write(&PORTB,PORTB5,1);
                _delay_ms(BLINK_DELAY_MS);

                digital_write(&PORTB,PORTB5,0);
                _delay_ms(BLINK_DELAY_MS);
        }

        return 0;
}
