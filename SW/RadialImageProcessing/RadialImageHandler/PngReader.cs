﻿using System;
using Cairo;
namespace RadialImageHandler
{
	public class PngReader
	{
		private byte[] data;
		private int stride;
		private int height;
		private int width;

		public PngReader(string file)
		{
			ImageSurface surf = new ImageSurface(file);
			data = surf.Data;
			stride = surf.Stride;
			height = surf.Height;
			width = surf.Width;
			surf.Dispose();
		}

		public static void WriteImage(string file, CartesianImage pixels)
		{
			ImageSurface surf = new ImageSurface(Format.ARGB32, pixels.Width, pixels.Height);
			Context c = new Context(surf);
			foreach (CartesianImage.CartesianPixel p in pixels.pixels)
			{
				c.SetSourceRGB(p.Strength, p.Strength, p.Strength);
				Cairo.Rectangle rect = new Rectangle(p.X, p.Y, 1, 1);
				c.Rectangle(rect);
				c.Fill();
			}
			surf.WriteToPng(file);
		}

		public static void WriteImage(string file, RadialImage radial, int width, int height)
		{
			ImageSurface surf = new ImageSurface(Format.ARGB32, radial.Width, radial.Height);
			Context c = new Context(surf);
			for (int i = 0; i < radial.pixels.Count; i++)
			{
				c.MoveTo(new PointD(radial.pixels[i].Points.Item1.X + (width / 2), (-1 * radial.pixels[i].Points.Item1.Y) + (height / 2)));
				c.LineTo(new PointD(radial.pixels[i].Points.Item2.X + (width / 2), (-1 * radial.pixels[i].Points.Item2.Y) + (height / 2)));
				c.LineTo(new PointD(radial.pixels[i].Points.Item3.X + (width / 2), (-1 * radial.pixels[i].Points.Item3.Y) + (height / 2)));
				c.LineTo(new PointD(radial.pixels[i].Points.Item4.X + (width / 2), (-1 * radial.pixels[i].Points.Item4.Y) + (height / 2)));
				c.ClosePath();
				c.SetSourceRGB(radial.pixels[i].Strength, radial.pixels[i].Strength, radial.pixels[i].Strength);
				c.Fill();
			}
			surf.WriteToPng(file);
		}

		public CartesianImage getPixels()
		{
			CartesianImage img = new CartesianImage(width,height);
			int pixrow = stride / 4;
			int pix = 0;
			for (int i = 0; i < data.Length; i+=4)
			{
				img.AddPixel(pix%pixrow, (int)Math.Truncate((double)pix/(double)pixrow), (double)data[i]/Byte.MaxValue);
				pix++;
			}
			return img;
		}
	}
}
