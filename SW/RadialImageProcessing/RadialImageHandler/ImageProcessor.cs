﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace RadialImageHandler
{
	public class ImageProcessor
	{
		public enum SamplingMethod { Radial, NearestNeighbour, WeightedAverage, RBF };

		private Parameters parameters;
		private BackgroundWorker bw;

		public ImageProcessor(Parameters parameters)
		{
			this.parameters = parameters;
		}

		public CartesianImage Run(BackgroundWorker bw)
		{
			if (bw != null)
				this.bw = bw;
			
			switch (parameters.method)
			{
				case SamplingMethod.RBF:
					RBFParameters par1 = (RBFParameters)parameters;
					return RBF(par1.rad, par1.width, par1.height, par1.BRadius, par1.NLayers, par1.vLambda, true);
				case SamplingMethod.WeightedAverage:
					WAParameters par2 = (WAParameters)parameters;
					return WeightedAverage(par2.rad, par2.width, par2.height, par2.radius, par2.powerFactor, false);
				default:
					NNParameters par3 = (NNParameters)parameters;
					return NearestNeighbour(par3.rad, par3.width, par3.height, false);
			}
		}

		private void ReportProgress(double fraction, string status)
		{
			if (bw != null)
				bw.ReportProgress(Convert.ToInt32(fraction * 100), status);
		}

		public CartesianImage RBF(RadialImage rad, int width, int height, int BRadius, int NLayers, double? vLambda, bool onlyValued)
		{
			alglib.rbfmodel model;
			alglib.rbfcreate(2, 1, out model);
			double[,] points = new double[rad.pixels.Where(q => (q.Strength > 0 || !onlyValued)).Count(), 3];
			int x = 0;
			ReportProgress(0, "Collecting points");
			try
			{
				for (int i = 0; i < rad.pixels.Count; i++)
				{
					x = i;
					RadialImage.RadialPixel p = rad.pixels[i];
					if (p.Strength > 0 || !onlyValued)
					{
						Tuple<double, double> sc = p.center.ScreenCoordinate(height, width);
						points[i, 0] = sc.Item1;
						points[i, 1] = sc.Item2;
						points[i, 2] = p.Strength;
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
			alglib.rbfsetpoints(model, points);
			alglib.rbfreport report;

			if (vLambda.HasValue)
				alglib.rbfsetalgomultilayer(model, BRadius, NLayers, vLambda.Value);
			else
				alglib.rbfsetalgomultilayer(model, BRadius, NLayers);

			ReportProgress(0, "Building Model");
			alglib.rbfbuildmodel(model, out report);

			ReportProgress(0, "Calculating values");
			List<CartesianImage.CartesianPixel> pix = new List<CartesianImage.CartesianPixel>();
			for (int i = 0; i < width; i++)
			{
				for (int j = 0; j < height; j++)
				{
					pix.Add(new CartesianImage.CartesianPixel(i, j, alglib.rbfcalc2(model, i, j)));
					ReportProgress(((double)(i*width)+j)/(width*height), "Calculating values");
				}
			}
			ReportProgress(1, "Drawing!");
			return new CartesianImage(width, height, pix);
		}

		public CartesianImage WeightedAverage(RadialImage rad, int width, int height, double radius, double powerFactor, bool onlyValued)
		{
			List<CartesianImage.CartesianPixel> pixels = new List<CartesianImage.CartesianPixel>();
			for (int i = 0; i < width; i++)
			{
				for (int j = 0; j < height; j++)
				{
					Coordinate c = new Coordinate();
					c.X = i - (width / 2);
					c.Y = (-1 * j) + (height / 2);
					double s = 0;
					s = rad.GetAvgNearest(c, radius, powerFactor, onlyValued);
					CartesianImage.CartesianPixel pix = new CartesianImage.CartesianPixel(i, j, s);
					pixels.Add(pix);
					ReportProgress(((double)pixels.Count / (double)(height * width)), "Calculating values!");
				}
			}
			return new CartesianImage(width, height, pixels);
		}

		public CartesianImage NearestNeighbour(RadialImage rad, int width, int height, bool onlyValued)
		{
			List<CartesianImage.CartesianPixel> pixels = new List<CartesianImage.CartesianPixel>();
			for (int i = 0; i < width; i++)
			{
				for (int j = 0; j < height; j++)
				{
					Coordinate c = new Coordinate();
					c.X = i - (width / 2);
					c.Y = (-1 * j) + (height / 2);
					double s = 0;
					s = rad.GetClosest(c, onlyValued);
					CartesianImage.CartesianPixel pix = new CartesianImage.CartesianPixel(i, j, s);
					pixels.Add(pix);
					ReportProgress(((double)pixels.Count / (double)(height * width)), "Calculating values!");
				}
			}
			return new CartesianImage(width, height, pixels);
		}

		public static RadialImage Radial(CartesianImage cart, int degSteps, int sensorCount, double sensorHeight, double sensorWidth, double radialOffset)
		{
			List<RadialImage.RadialPixel> pixels = new List<RadialImage.RadialPixel>();

			double w = sensorWidth;
			double h = sensorHeight;
			double o = radialOffset;
			double s = (((double)cart.Height / 2) - (sensorCount * sensorHeight) - radialOffset) / (sensorCount - 1);

			List<Tuple<Coordinate, Coordinate, Coordinate, Coordinate>> points = new List<Tuple<Coordinate, Coordinate, Coordinate, Coordinate>>();

			for (int j = 0; j < sensorCount; j++)
			{
				Coordinate p1 = new Coordinate();
				Coordinate p2 = new Coordinate();
				Coordinate p3 = new Coordinate();
				Coordinate p4 = new Coordinate();
				p1.X = -1 * w / 2;
				p1.Y = o + (j * h) + (j * s);
				p2.X = w / 2;
				p2.Y = o + ((j + 1) * h) + (j * s);
				p3.X = p1.X;
				p3.Y = p2.Y;
				p4.X = p2.X;
				p4.Y = p1.Y;

				points.Add(new Tuple<Coordinate, Coordinate, Coordinate, Coordinate>(p1, p2, p3, p4));
			}

			double strength = 1;
			double a = (Math.PI * 2) / degSteps;
			for (int i = 0; i < degSteps; i++)
			{
				for (int j = 0; j < sensorCount; j++)
				{
					points[j].Item1.T += a;
					points[j].Item2.T += a;
					points[j].Item3.T += a;
					points[j].Item4.T += a;
					Tuple<Coordinate, Coordinate, Coordinate, Coordinate> tup = new Tuple<Coordinate, Coordinate, Coordinate, Coordinate>(points[j].Item1, points[j].Item3, points[j].Item2, points[j].Item4);
					strength = cart.GetAverageWithinRectangle(points[j].Item1, points[j].Item3, points[j].Item2, points[j].Item4);
					RadialImage.RadialPixel pix = new RadialImage.RadialPixel(a, o + (j * h) + (j * s), strength, tup);
					pixels.Add(pix);
				}
			}

			return new RadialImage(sensorWidth, sensorHeight, radialOffset, sensorCount, degSteps, pixels);
		}
	}

	public interface Parameters {
		ImageProcessor.SamplingMethod method { get; }
	}
	public class RadialParameters : Parameters
	{
		public CartesianImage cart;
		public int degSteps;
		public int sensorCount;
		public double sensorHeight;
		public double sensorWidth;
		public double radialOffset;

		public ImageProcessor.SamplingMethod method
		{
			get
			{
				return ImageProcessor.SamplingMethod.Radial;
			}
		}

		public RadialParameters(CartesianImage cart, int degSteps, int sensorCount, double sensorHeight, double sensorWidth, double radialOffset)
		{
			this.cart = cart;
			this.degSteps = degSteps;
			this.sensorCount = sensorCount;
			this.sensorHeight = sensorHeight;
			this.sensorWidth = sensorWidth;
			this.radialOffset = radialOffset;
		}
	}
	public class NNParameters : Parameters
	{
		public RadialImage rad;
		public int width;
		public int height;

		public ImageProcessor.SamplingMethod method
		{
			get
			{
				return ImageProcessor.SamplingMethod.NearestNeighbour;
			}
		}

		public NNParameters(RadialImage rad, int width, int height)
		{
			this.rad = rad;
			this.width = width;
			this.height = height;
		}
	}
	public class WAParameters : Parameters
	{
		public RadialImage rad;
		public int width;
		public int height; 
		public double radius; 
		public double powerFactor;

		public ImageProcessor.SamplingMethod method
		{
			get
			{
				return ImageProcessor.SamplingMethod.WeightedAverage;
			}
		}

		public WAParameters(RadialImage rad, int width, int height, double radius, double powerFactor)
		{
			this.rad = rad;
			this.width = width;
			this.height = height;
			this.radius = radius;
			this.powerFactor = powerFactor;
		}
	}
	public class RBFParameters : Parameters
	{
		public RadialImage rad;
		public int width;
		public int height;
		public int BRadius;
		public int NLayers;
		public double? vLambda;

		public ImageProcessor.SamplingMethod method
		{
			get
			{
				return ImageProcessor.SamplingMethod.RBF;
			}
		}

		public RBFParameters(RadialImage rad, int width, int height, int BRadius, int NLayers, double? vLambda)
		{
			this.rad = rad;
			this.width = width;
			this.height = height;
			this.BRadius = BRadius;
			this.NLayers = NLayers;
			this.vLambda = vLambda;
		}
	}
}
