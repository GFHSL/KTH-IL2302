﻿#define dummydat

using System;
using System.IO.Ports;
using System.Timers;
using System.Collections.Generic;



namespace RadialImageHandler
{
	public class COMReader
	{
		private int dummyOne = 0;
		private int dummyTwo = 0;

		private Random random = new Random();

		private List<RadialImage.RadialPixel> allPixels = new List<RadialImage.RadialPixel>();

		public delegate void DataReceivedEventHandler(object sender, PixelEventArgs args);

		public event DataReceivedEventHandler DataReceived;

		public COMReader()
		{
			allPixels = new List<RadialImage.RadialPixel>();

			SerialPort port = new SerialPort("/dev/ttyACM0", 9600);

#if (!dummydat)
			port.Open();
#endif

			port.ReadTimeout = 400;

			Timer t = new Timer(200);
			t.Elapsed += (sender, e) =>
			{
#if !dummydat
				char[] bytes = new char[port.BytesToRead];
				int i = 0;
				while (port.BytesToRead > 0)
				{
					bytes[i] = (char)port.ReadByte();
					i++;
				}
				string dat = new String(bytes);
#else

				if (dummyOne > 14)
				{
					dummyOne = -1;
					dummyTwo += 10;
				}
				dummyOne++;
				string dat = "";
				if (dummyTwo < 360)
					dat = dummyTwo.ToString() + ";" + dummyOne.ToString() + ";" + random.Next(0, 1024).ToString();
#endif
				string[] lines = dat.Split('\n');
				List<RadialImage.RadialPixel> pixels = new List<RadialImage.RadialPixel>();
				foreach (string l in lines)
				{
					RadialImage.RadialPixel p = parseLine(l);
					if (p != null)
						pixels.Add(p);
				}
				if (DataReceived != null)
				{
					allPixels.AddRange(pixels);
					PixelEventArgs args = new PixelEventArgs(allPixels);
					DataReceived(this, args);
				}
			};
			t.Start();
		}

		public RadialImage.RadialPixel parseLine(string line)
		{
			string[] parms = line.Split(';');
			try
			{
				double strength = Convert.ToDouble(parms[2]) / 1024.0;
				RadialImage.RadialPixel pix = new RadialImage.RadialPixel(Convert.ToDouble(parms[0]), Convert.ToDouble(parms[1]), strength, RadialImage.RadialPixel.LocToPoints(Convert.ToInt32(parms[1]), Convert.ToDouble(parms[0]), 16, 400, 10, 10));
				return pix;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
			return null;
		}

	}

	public class PixelEventArgs : EventArgs
	{
		public List<RadialImage.RadialPixel> pixels;
		public PixelEventArgs(List<RadialImage.RadialPixel> pixels)
		{
			this.pixels = pixels;
		}
	}
}
