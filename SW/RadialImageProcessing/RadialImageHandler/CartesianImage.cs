﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RadialImageHandler
{
	public class CartesianImage
	{
		

		private List<CartesianPixel> _pixels;
		public List<CartesianPixel> pixels
		{
			get
			{
				return _pixels;
			}
			set
			{
				_pixels = value;
				pixelsDirty = true;
			}
		}
		private bool pixelsDirty;

		private CartesianPixel[][] dimPixels;
		public CartesianPixel[][] DimPixels
		{
			get
			{
				if (pixelsDirty)
				{
					dimPixels = new CartesianPixel[Width][];
					for (int i = 0; i < Width; i++)
					{
						dimPixels[i] = new CartesianPixel[Height];
						for (int j = 0; j < Height; j++)
						{
							dimPixels[i][j] = pixels[i * Width + j];
						}
					}
					pixelsDirty = false;
				}
				return dimPixels;
			}
		}

		public int Width;
		public int Height;

		public CartesianImage(int Width, int Height)
		{
			pixels = new List<CartesianPixel>();
			this.Width = Width;
			this.Height = Height;
		}

		public CartesianImage(int Width, int Height, List<CartesianPixel> pixels)
		{
			this.Width = Width;
			this.Height = Height;
			this.pixels = pixels;
		}

		public void AddPixel(int x, int y, double strength)
		{
			pixels.Add(new CartesianPixel(x, y, strength));
		}

		public CartesianPixel GetPixel(int x, int y)
		{
				if (y * Width + x >= pixels.Count)
					return pixels[pixels.Count - 1];
				return pixels[y * Width + x];
		}

		public double GetAverageWithinRectangle(Coordinate p1, Coordinate p2, Coordinate p3, Coordinate p4)
		{
			List<Coordinate> ps = new List<Coordinate>();
			ps.Add(p1);
			ps.Add(p2);
			ps.Add(p3);
			ps.Add(p4);
			double mY = Math.Floor((Height/2) - ps.Min(p => p.Y));
			double MY = Math.Ceiling((Height/2) - ps.Max(p => p.Y));
			double mX = Math.Ceiling(ps.Min(p => p.X) + (Width/2));
			double MX = Math.Floor(ps.Max(p => p.X) + (Width/2));
			double sum = 0;
			int count = 0;
			for (int i = Convert.ToInt32(MY); i < mY; i++)
			{
				for (int j = Convert.ToInt32(mX); j < MX; j++)
				{
					CartesianPixel p = DimPixels[i][j];
					//if (PointWithinRectangle(p.X - (Width / 2), -1 * (p.Y - (Height / 2)), mX, MX, mY, MY))
					//{
						sum += p.Strength;
						count += 1;
					//}
				}
			}
			/*
			foreach (CartesianPixel p in pixels)
			{
				if (PointWithinRectangle(p.X-(Width/2), -1*(p.Y-(Height/2)), mX, MX, mY, MY))
				{
					sum += p.Strength;
					count += 1;
				}
			}*/
			return sum / count;
		}

		public bool PointWithinRectangle(double X, double Y, double mX, double MX, double mY, double MY)
		{
			return X > mX && X < MX && Y > mY && Y < MY;
		}

		public class CartesianPixel
		{
			public int X;
			public int Y;
			public double Strength;

			public CartesianPixel(int x, int y, double strength)
			{
				this.X = x;
				this.Y = y;
				this.Strength = strength;
			}
		}


	}
}
