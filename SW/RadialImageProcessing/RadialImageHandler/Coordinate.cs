﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RadialImageHandler
{
	public class Coordinate
	{
		public enum Representation { Radial, Cartesian };

		public Representation system;

		private double _x = 0;
		private double _y = 0;
		private double _r = 0;
		private double _t = 0;

		public double X
		{
			get
			{
				return _x;
			}
			set
			{
				_x = value;
				recalculateRadial();
			}
		}

		public double Y
		{
			get
			{
				return _y;
			}
			set
			{
				_y = value;
				recalculateRadial();
			}
		}

		public double R
		{
			get
			{
				return _r;
			}
			set
			{
				_r = value;
				recalculateCartesian();
			}
		}

		public double T
		{
			get
			{
				return _t;
			}
			set
			{
				_t = value;
				recalculateCartesian();
			}
		}

		public Coordinate(double x, double y)
		{
			this._y = y;
			this._x = x;
			recalculateRadial();
		}

		public static Coordinate fromRad(double t, double r)
		{
			Coordinate ret = new Coordinate();
			ret._t = t;
			ret._r = r;
			ret.recalculateCartesian();
			return ret;
		}

		public Coordinate() { }

		public Tuple<double, double> ScreenCoordinate(int height, int width)
		{
			double sx = X + (width / 2);
			double sy = (height / 2) - Y;
			Tuple<double, double> ret = new Tuple<double, double>(sx, sy);
			return ret;
		}

		private void recalculateRadial()
		{
			_r = Math.Sqrt(Math.Pow(_x, 2) + Math.Pow(_y, 2));
			_t = Math.Atan2(_y, _x);
		}

		private void recalculateCartesian()
		{
			_x = _r * Math.Cos(_t);
			_y = _r * Math.Sin(_t);
		}

		public override string ToString()
		{
			return string.Format("{0}|{1}", X, Y);
		}

		public double DistanceTo(Coordinate a)
		{
			return Distance(this, a);
		}

		public static double Distance(Coordinate a, Coordinate b)
		{
			return Math.Sqrt(Math.Pow(a.X-b.X,2) + Math.Pow(a.Y-b.Y, 2));
		}

		public Coordinate Copy()
		{
			return new Coordinate(X, Y);
		}
	}
}
