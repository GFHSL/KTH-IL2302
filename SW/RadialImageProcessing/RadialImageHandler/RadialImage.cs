﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RadialImageHandler
{
	public class RadialImage
	{
		public List<RadialPixel> _pixels;

		public List<RadialPixel> pixels
		{
			get
			{
				return _pixels;
			}
			set
			{
				_pixels = value;
				pixelsDirty = true;
			}
		}
		private bool pixelsDirty = true;

		private RadialPixel[][] dimPixels;
		public RadialPixel[][] DimPixels
		{
			get
			{
				if (pixelsDirty)
				{
					dimPixels = new RadialPixel[angleSteps][];
					for (int i = 0; i < angleSteps; i++)
					{
						dimPixels[i] = new RadialPixel[sensorCount];
						for (int j = 0; j < sensorCount; j++)
						{
							dimPixels[i][j] = pixels[i * sensorCount + j];
						}
					}
					pixelsDirty = false;
				}
				return dimPixels;
			}
		}

		private double sensorHeight;
		private double sensorWidth;
		private double sensorOffset;
		private int sensorCount;
		private int angleSteps;

		public int Height
		{
			get
			{
				RadialPixel top = pixels.OrderBy(p => p.getCenter().Y).First();
				RadialPixel bottom = pixels.OrderBy(p => p.getCenter().Y).Last();
				return Convert.ToInt32(Math.Abs(top.getCenter().Y - bottom.getCenter().Y) + sensorHeight);
			}
		}

		public int Width
		{
			get
			{
				RadialPixel left = pixels.OrderBy(p => p.getCenter().X).First();
				RadialPixel right = pixels.OrderBy(p => p.getCenter().X).Last();
				return Convert.ToInt32(Math.Abs(left.getCenter().X - right.getCenter().X) + sensorWidth);
			}
		}

		public RadialImage()
		{
			
		}

		public RadialImage(string radialFile, double sensorWidth, double sensorHeight, double sensorOffset, int sensorCount, int angleSteps)
		{
			FileStream fs = new FileStream(radialFile, FileMode.Open);
			StreamReader sr = new StreamReader(fs);
			string fullFile = sr.ReadToEnd();
			sr.Close();
			fs.Close();

			string[] lines = fullFile.Split('\n');
			List<RadialPixel> ipix = new List<RadialPixel>();
			foreach (string l in lines)
			{
				try
				{
					if (l.Length > 5)
						ipix.Add(parseLine(l, 45, 60, 1023));
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.ToString());
				}
			}

			double max = ipix.Select(p => p.Strength).Max();
			foreach (RadialPixel pix in ipix)
			{
				pix.Strength = (pix.Strength / max);
			}

			this.sensorWidth = sensorWidth;
			this.sensorHeight = sensorHeight;
			this.sensorCount = sensorCount;
			this.sensorOffset = sensorOffset;
			this.angleSteps = angleSteps;
			this.pixels = ipix;
		}

		private RadialImage.RadialPixel parseLine(string line, int spacing, int offset, double fullStrength)
		{
			string[] parms = line.Split(';');
			try
			{
				double angle = Convert.ToDouble(parms[0]);
				if (angle > 180)
				{
					angle = angle - 180;
					angle = 180 - angle;
					angle = angle + 180;
				}
				angle = (angle * Math.PI / 180);

				double strength = Convert.ToDouble(parms[2]) / fullStrength;
				strength = strength > 1023 ? 1023 : strength;
				double outset = offset + (spacing * Convert.ToDouble(parms[1]));
				RadialImage.RadialPixel pix = new RadialImage.RadialPixel(angle, outset, strength, RadialImage.RadialPixel.LocToPoints(angle, outset, 8, 400, 10, 10));
				return pix;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
			return null;
		}

		/*
		public RadialImage(CartesianImage cart, int degSteps, int sensorCount, double sensorHeight, double sensorWidth, double radialOffset)
		{
			
		}
		*/

		public RadialImage(double sensorWidth, double sensorHeight, double sensorOffset, int sensorCount, int angleSteps, List<RadialPixel> pixels)
		{
			this.sensorWidth = sensorWidth;
			this.sensorHeight = sensorHeight;
			this.sensorCount = sensorCount;
			this.sensorOffset = sensorOffset;
			this.angleSteps = angleSteps;
			this.pixels = pixels;
		}

		public double GetAvgNearest(Coordinate pixel, double rad, double powerFactor, bool onlyValued)
		{
			double sum = 0;
			double weights = 0;

			double a = Math.Atan(rad / pixel.R);
			List<RadialPixel[]> radii = new List<RadialPixel[]>();
			for (int i = 0; i < angleSteps; i++)
			{
				double ia = DimPixels[i][0].getCenter().T;
				double ian = ia + (Math.PI * 2);
				double ial = ia - (Math.PI * 2);

				if ((ia > (pixel.T - a) && ia < (pixel.T + a)) || (ian > (pixel.T - a) && ian < (pixel.T + a)) || (ial > (pixel.T - a) && ial < (pixel.T + a)))
				{
					radii.Add(DimPixels[i]);
				}
			}
			double w = 0;
			foreach (RadialPixel[] pa in radii)
			{
				for (int i = 0; i < sensorCount; i++)
				{
					if (pa[i].Strength > 0 || !onlyValued)
					{
						if (pa[i].getCenter().R > pixel.R - rad && pa[i].getCenter().R < pixel.R + rad)
						{
							w = Math.Pow(1 / (pa[i].getCenter().DistanceTo(pixel)), powerFactor);
							sum += pa[i].Strength * w;
							weights += w;
						}
					}
				}
			}
			return sum / weights;
		}

		public double GetClosest(Coordinate c, bool onlyValued)
		{
			double ret = 0;
			double dist = double.MaxValue;
			double nd = 0;

			int a = 0;
			for (int i = 0; i < DimPixels.Count(); i++)
			{
				if (Coordinate.Distance(DimPixels[i][0].getCenter(), c) < dist)
				{
					dist = Coordinate.Distance(DimPixels[i][0].getCenter(), c);
					a = i;
				}
			}

			dist = double.MaxValue;
			int b = a == 0 ? DimPixels.Count() - 1 : a - 1;
			int d = a == DimPixels.Count() - 1 ? 0 : a + 1;

			foreach (RadialPixel pix in DimPixels[a])
			{
				nd = Coordinate.Distance(c, pix.center);
				if (nd < dist && (pix.Strength > 0 || !onlyValued))
				{
					dist = nd;
					ret = pix.Strength;
				}
			}
			foreach (RadialPixel pix in DimPixels[b])
			{
				nd = Coordinate.Distance(c, pix.center);
				if (nd < dist && (pix.Strength > 0 || !onlyValued))
				{
					dist = nd;
					ret = pix.Strength;
				}
			}
			foreach (RadialPixel pix in DimPixels[d])
			{
				nd = Coordinate.Distance(c, pix.center);
				if (nd < dist && (pix.Strength > 0 || !onlyValued))
				{
					dist = nd;
					ret = pix.Strength;
				}
			}
			return ret;
		}

		public static void GenerateTestFile(string filename)
		{
			Random rnd = new Random();
			FileStream fs = new FileStream(filename, FileMode.Create);
			StreamWriter sw = new StreamWriter(fs);
			for (int i = 0; i < 360; i += 10)
			{
				for (int j = 0; j < 16; j++)
				{
					sw.WriteLine(j.ToString() + ";" + i.ToString() + ";" + rnd.Next(0, 1024).ToString());
				}
			}
			sw.Close();
			fs.Close();
		}

		public class RadialPixel
		{
			public double Angle;
			public double Radius;
			public double Strength;

			public Coordinate center;

			public Tuple<Coordinate, Coordinate, Coordinate, Coordinate> Points;
			private List<Coordinate> pointList;

			public RadialPixel(double angle, double radius, double strength, Tuple<Coordinate, Coordinate, Coordinate, Coordinate> points)
			{
				this.Angle = angle;
				this.Radius = radius;
				this.Strength = strength;
				this.Points = new Tuple<Coordinate, Coordinate, Coordinate, Coordinate>(points.Item1.Copy(), points.Item2.Copy(), points.Item3.Copy(), points.Item4.Copy());
				this.pointList = new List<Coordinate>();
				pointList.Add(points.Item1.Copy());
				pointList.Add(points.Item2.Copy());
				pointList.Add(points.Item3.Copy());
				pointList.Add(points.Item4.Copy());

				double mY = pointList.Min(p => p.Y);
				double MY = pointList.Max(p => p.Y);
				double mX = pointList.Min(p => p.X);
				double MX = pointList.Max(p => p.X);

				center = new Coordinate();
				center.X = (MX + mX) / 2;
				center.Y = (MY + mY) / 2;
			}

			public Coordinate getCenter()
			{
				return center;
			}

			public static Tuple<Coordinate, Coordinate, Coordinate, Coordinate> LocToPoints(double angle, double pos, int count, int fullRadius, double width, double height)
			{
				Coordinate center = Coordinate.fromRad(angle, pos);
				center.X -= width / 2;
				center.Y -= height / 2;
				Coordinate p1 = center.Copy();
				center.X += width;
				Coordinate p2 = center.Copy();
				center.Y += height;
				Coordinate p3 = center.Copy();
				center.X -= width;
				Coordinate p4 = center.Copy();
				return new Tuple<Coordinate, Coordinate, Coordinate, Coordinate>(p1, p2, p3, p4);
			}
		}


	}
}
