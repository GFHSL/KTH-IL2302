﻿using System;
namespace RadialImageDrawer
{
	public partial class CartesianSampling : Gtk.Dialog
	{
		public RadialImageHandler.ImageProcessor.SamplingMethod samplingMethod = RadialImageHandler.ImageProcessor.SamplingMethod.NearestNeighbour;
		public double radius = 100;
		public double powerFactor = 2;

		public int BRadius = 20;
		public int NLayers = 5;
		public double vLambda = 0.001;
		public bool useLambda = true;

		public CartesianSampling()
		{
			this.Build();
			tabWeightedAverage.Visible = false;
			tableRBF.Visible = false;
		}

		protected void OnCmbMethodChanged(object sender, EventArgs e)
		{
			tabWeightedAverage.Visible = false;
			tableRBF.Visible = false;
			switch (cmbMethod.Active)
			{
				case 0:
					samplingMethod = RadialImageHandler.ImageProcessor.SamplingMethod.NearestNeighbour;
					break;
				case 1:
					samplingMethod = RadialImageHandler.ImageProcessor.SamplingMethod.WeightedAverage;
					tabWeightedAverage.Visible = true;
					break;
				case 2:
					samplingMethod = RadialImageHandler.ImageProcessor.SamplingMethod.RBF;
					tableRBF.Visible = true;
					break;
				default:
					samplingMethod = RadialImageHandler.ImageProcessor.SamplingMethod.NearestNeighbour;
					break;
			}
		}

		protected void OnEntryRadiusChanged(object sender, EventArgs e)
		{
			if (entryRadius.Text.Length > 0)
				radius = Convert.ToDouble(entryRadius.Text);
		}

		protected void OnEntryPowerChanged(object sender, EventArgs e)
		{
			if (entryPower.Text.Length > 0)
				powerFactor = Convert.ToDouble(entryPower.Text);
		}

		protected void OnEntryRBaseChanged(object sender, EventArgs e)
		{
			if (entryRBase.Text.Length > 0)
				BRadius = Convert.ToInt32(entryRBase.Text);
		}

		protected void OnEntryNLayersChanged(object sender, EventArgs e)
		{
			if (entryNLayers.Text.Length > 0)
				NLayers = Convert.ToInt32(entryNLayers.Text);
		}

		protected void OnEntryVLambdaChanged(object sender, EventArgs e)
		{
			if (entryVLambda.Text.Length > 0)
				vLambda = Convert.ToDouble(entryVLambda.Text);
		}

		protected void OnChkLambdaToggled(object sender, EventArgs e)
		{
			useLambda = chkLambda.Active;
		}
	}
}
