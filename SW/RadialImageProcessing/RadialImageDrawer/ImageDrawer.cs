﻿using System;
using System.ComponentModel;
using RadialImageHandler;

namespace RadialImageDrawer
{
	public partial class ImageDrawer : Gtk.Window
	{
		public ImageProcessor.SamplingMethod method;
		public Parameters processingParameters;
		public RadialImage RadialSource;
		public CartesianImage CartesianSource;

		private BackgroundWorker worker;

		public ImageDrawer() :
				base(Gtk.WindowType.Toplevel)
		{
			this.Build();

			worker = new BackgroundWorker();
			worker.DoWork += DoBackgroundWork;
			worker.ProgressChanged += ProgressChanged;
			worker.RunWorkerCompleted += WorkerCompleted;
		}

		private void WorkerCompleted(object sender, RunWorkerCompletedEventArgs args)
		{

		}

		private void DoBackgroundWork(object sender, DoWorkEventArgs args)
		{

		}

		private void ProgressChanged(object sender, ProgressChangedEventArgs args)
		{

		}
	}
}
