
// This file has been generated by the GUI designer. Do not modify.
namespace RadialImageDrawer
{
	public partial class ImageDrawer
	{
		protected virtual void Build()
		{
			global::Stetic.Gui.Initialize(this);
			// Widget RadialImageDrawer.ImageDrawer
			this.Name = "RadialImageDrawer.ImageDrawer";
			this.Title = global::Mono.Unix.Catalog.GetString("ImageDrawer");
			this.WindowPosition = ((global::Gtk.WindowPosition)(4));
			if ((this.Child != null))
			{
				this.Child.ShowAll();
			}
			this.DefaultWidth = 400;
			this.DefaultHeight = 300;
			this.Show();
		}
	}
}
