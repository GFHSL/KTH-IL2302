﻿
using System;
using Gtk;
using Cairo;
using RadialImageHandler;
using System.Threading;
using RadialImageDrawer;
using System.ComponentModel;

public partial class MainWindow : Gtk.Window
{
	private static CartesianImage mainCartesian;
	private static Parameters processingParameters;
	private static RadialImage mainRadial;
	private BackgroundWorker bw;
	private bool isAtRadial = false;

	DrawingArea area;
	Context c;

	public MainWindow() : base(Gtk.WindowType.Toplevel)
	{
		this.SetSizeRequest(800, 800 + 50);

		Build();

		mainDrawing.ExposeEvent += OnExpose;
		bw = new BackgroundWorker();
		bw.DoWork += DoBackgroundWork;
		bw.ProgressChanged += ProgressChanged;
		bw.RunWorkerCompleted += WorkerCompleted;
		bw.WorkerReportsProgress = true;
	}

	void OnExpose(object sender, ExposeEventArgs args)
	{
	}

	protected void OnDeleteEvent(object sender, DeleteEventArgs a)
	{
		Application.Quit();
		a.RetVal = true;
	}

	private void WorkerCompleted(object sender, RunWorkerCompletedEventArgs args)
	{
		Gtk.Application.Invoke(delegate
		{
			DrawImage(mainCartesian);
		});
	}

	private void DoBackgroundWork(object sender, DoWorkEventArgs args)
	{
		ImageProcessor proc = new ImageProcessor(processingParameters);
		CartesianImage c = proc.Run(bw);
		mainCartesian = c;
	}

	int percent = 0;
	private void ProgressChanged(object sender, ProgressChangedEventArgs args)
	{
		if (args.ProgressPercentage != percent)
		{
			Gtk.Application.Invoke(delegate
			{
				this.progress.Fraction = (double)args.ProgressPercentage / 100.0;
				this.progress.Text = args.UserState != null ? (string)args.UserState : args.ProgressPercentage.ToString() + "%";
			});
			//this.progress.Text = args.ProgressPercentage.ToString();

		}
	}

	private void DrawRadialImage(RadialImage radial)
	{
		try
		{
			isAtRadial = true;
			//mainRadial = radial;
			if (area == null)
				area = mainDrawing;
			if (c == null)
			{
				c = Gdk.CairoHelper.Create(area.GdkWindow);
				c.SetSourceRGB(1, 1, 1);
				c.Paint();
			}


			for (int i = 0; i < radial.pixels.Count; i++)
			{
				c.MoveTo(new PointD(radial.pixels[i].Points.Item1.X + 400, (-1 * radial.pixels[i].Points.Item1.Y) + 400));
				c.LineTo(new PointD(radial.pixels[i].Points.Item2.X + 400, (-1 * radial.pixels[i].Points.Item2.Y) + 400));
				c.LineTo(new PointD(radial.pixels[i].Points.Item3.X + 400, (-1 * radial.pixels[i].Points.Item3.Y) + 400));
				c.LineTo(new PointD(radial.pixels[i].Points.Item4.X + 400, (-1 * radial.pixels[i].Points.Item4.Y) + 400));
				c.ClosePath();
				c.SetSourceRGB(radial.pixels[i].Strength, radial.pixels[i].Strength, radial.pixels[i].Strength);
				c.Fill();
			}

			/*
			foreach (RadialImage.RadialPixel p in radial.pixels)
			{
				c.SetSourceRGB(p.Strength, p.Strength, p.Strength);
				Cairo.Rectangle rect = new Rectangle(p.getCenter().X + 400, (-1 * p.getCenter().Y) + 400, 1, 1);
				c.Rectangle(rect);
				c.Fill();
			}*/
		}
		catch (Exception ex)
		{
			Console.WriteLine(ex.ToString());
		}
	}

	private void DrawImage(CartesianImage cart)
	{
		mainCartesian = cart;
		DrawMainCartesian();
	}

	private void DrawMainCartesian()
	{
		isAtRadial = false;
		DrawingArea area = mainDrawing;
		Context c = Gdk.CairoHelper.Create(area.GdkWindow);
		foreach (CartesianImage.CartesianPixel p in mainCartesian.pixels)
		{
			c.SetSourceRGB(p.Strength, p.Strength, p.Strength);
			Cairo.Rectangle rect = new Rectangle(p.X, p.Y, 1, 1);
			c.Rectangle(rect);
			c.Fill();
		}
		((IDisposable)c.GetTarget()).Dispose();
		((IDisposable)c).Dispose();
		((IDisposable)c).Dispose();
	}

	protected void OnLoadPngFileActionActivated(object sender, EventArgs e)
	{
		Gtk.FileChooserDialog chooser = new Gtk.FileChooserDialog("Choose a 800x800 PNG image",
																  this,
																  FileChooserAction.Open,
																 "Cancel", ResponseType.Cancel,
																  "Open", ResponseType.Accept);
		if (chooser.Run() == (int)ResponseType.Accept)
		{
			PngReader reader = new PngReader(chooser.Filename);
			chooser.Destroy();
			DrawImage(reader.getPixels());
		}
		else
		{
			chooser.Destroy();
		}
	}

	protected void OnSampleIntoRadialActionActivated(object sender, EventArgs e)
	{
		RadialSampling dial = new RadialSampling();
		if (dial.Run() == (int)ResponseType.Ok)
		{
			mainRadial = ImageProcessor.Radial(mainCartesian, dial.AngleSteps, dial.SensorCount, dial.SensorHeight, dial.SensorWidth, dial.SensorOffset);
			dial.Destroy();
			DrawRadialImage(mainRadial);
		}
		else
			dial.Destroy();
	}

	protected void OnSavePngFileActionActivated(object sender, EventArgs e)
	{
		Gtk.FileChooserDialog chooser = new Gtk.FileChooserDialog("Save as", this, FileChooserAction.Save, "Cancel", ResponseType.Cancel, "Save", ResponseType.Accept);
		if (chooser.Run() == (int)ResponseType.Accept)
		{
			string f = chooser.Filename;
			chooser.Destroy();
			if (isAtRadial)
				PngReader.WriteImage(f, mainRadial, 800, 800);
			else
				PngReader.WriteImage(f, mainCartesian);
		}
		else
			chooser.Destroy();
	}



	protected void OnSampleIntoCartesianActionActivated(object sender, EventArgs e)
	{
		CartesianSampling dial = new CartesianSampling();

		if (dial.Run() == (int)ResponseType.Ok)
		{
			ImageProcessor.SamplingMethod method = dial.samplingMethod;

			switch (method)
			{
				case ImageProcessor.SamplingMethod.WeightedAverage:
					processingParameters = new WAParameters(
						mainRadial,
						800,
						800,
						dial.radius,
						dial.powerFactor);
					bw.RunWorkerAsync();
					break;
				case ImageProcessor.SamplingMethod.NearestNeighbour:
					processingParameters = new NNParameters(
						mainRadial,
						800,
						800);
					bw.RunWorkerAsync();
					break;
				case ImageProcessor.SamplingMethod.RBF:
					double? r = null;
					if (dial.useLambda)
						r = dial.vLambda;
					processingParameters = new RBFParameters(
						mainRadial,
						800,
						800,
						dial.BRadius,
						dial.NLayers,
						r);
					bw.RunWorkerAsync();
					break;
			}
		}
		dial.Destroy();
	}

	protected void OnConnectPortActionActivated(object sender, EventArgs e)
	{
		RadialImageHandler.COMReader r = new COMReader();
		r.DataReceived += (isender, args) =>
		{
			if (args.pixels.Count > 5)
			{
				RadialImage rad = new RadialImage(20, 20, 5, 16, 36, args.pixels);
				mainRadial = rad;
				DrawRadialImage(mainRadial);
			}
		};
	}

	protected void OnLoadRadialFileActionActivated(object sender, EventArgs e)
	{
		Gtk.FileChooserDialog chooser = new Gtk.FileChooserDialog("Choose a radial image file",
																  this,
																  FileChooserAction.Open,
																 "Cancel", ResponseType.Cancel,
																  "Open", ResponseType.Accept);
		if (chooser.Run() == (int)ResponseType.Accept)
		{
			mainRadial = new RadialImage(chooser.Filename, 40, 40, 100, 8, 70);
			chooser.Destroy();
			DrawRadialImage(mainRadial);
		}
		else
		{
			chooser.Destroy();
		}
	}
}
