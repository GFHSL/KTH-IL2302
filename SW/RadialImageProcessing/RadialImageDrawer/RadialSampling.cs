﻿using System;
namespace RadialImageDrawer
{
	public partial class RadialSampling : Gtk.Dialog
	{
		public double SensorHeight = 20;
		public double SensorWidth = 20;
		public int SensorCount = 16;
		public double SensorOffset = 5;
		public int AngleSteps = 100;
		public RadialSampling()
		{
			this.Build();
		}

		protected void OnEntryHeightChanged(object sender, EventArgs e)
		{
			if (entryHeight.Text.Length > 0)
				SensorHeight = Convert.ToDouble(entryHeight.Text);
		}

		protected void OnEntryWidthChanged(object sender, EventArgs e)
		{
			if (entryWidth.Text.Length > 0)
				SensorWidth = Convert.ToDouble(entryWidth.Text);
		}

		protected void OnEntryCountChanged(object sender, EventArgs e)
		{
			if (entryCount.Text.Length > 0)
				SensorCount = Convert.ToInt32(entryCount.Text);
		}

		protected void OnEntryOffsetChanged(object sender, EventArgs e)
		{
			if (entryOffset.Text.Length > 0)
				SensorOffset = Convert.ToDouble(entryOffset.Text);
		}

		protected void OnEntryStepsChanged(object sender, EventArgs e)
		{
			if (entrySteps.Text.Length > 0)
				AngleSteps = Convert.ToInt32(entrySteps.Text);
		}
	}
}
