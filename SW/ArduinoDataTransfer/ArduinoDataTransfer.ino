
/*
 * This code assumes that the mux(digital pins 7-5) are connected to both arrays
 * 
 * ledPin = 13
 * analoginput(1) = A0
 * analoginput(2) = A1
 * PWM dutycycle = 50%
 * PWM pin = 9
 * mux pins = (digital)7-5 (3 pin mux)
 */
 
//#include "TimerOne.h"
#include <Servo.h>

#define ledPin 13
#define sensorPinA0 A0
#define sensorPinA1 A1

char activeFlag = 1;
unsigned int sumA0 = 0;
unsigned int sumA1 = 0;
unsigned char countSum = 0;
unsigned char muxSelect = 0;
int angle = 0;

Servo servoObj;

void setup()
{
  Serial.begin(115200);
  
  pinMode(ledPin, OUTPUT);
  //Timer1.initialize(1000000);         
  //Timer1.pwm(9, 4);                
  //Timer1.attachInterrupt(callback);  
  analogReference(INTERNAL);
  DDRD = (DDRD & B00011111) | B11100000;
  servoObj.attach(9);  // attaches the servo on pin 9 to the servo object

}
 
//void callback()
//{
//  digitalWrite(ledPin, digitalRead(ledPin) ^ 1);
//  activeFlag = 1;
//}
 
void loop()
{
  delay(100);
  for (angle = 0; angle <= 180; angle += 1) {
    Serial.print(angle);
    Serial.print(" ");
    servoObj.write(angle);
    delay(50);
      for(muxSelect=0; muxSelect < 8; muxSelect++)
      {
        sumA0 = 0;
        sumA1 = 0;
        PORTD = ((muxSelect & 7) << 5) | (PORTD & B00011111);
        delay(1);
        for(countSum=0;countSum<16;countSum++)
        {
          sumA0 += analogRead(sensorPinA0);
          sumA1 += analogRead(sensorPinA1);
          delay(1);
        }
        
        Serial.print(muxSelect);
        Serial.print(" ");
        Serial.print(sumA0 >> 4);
        Serial.print(" ");
        Serial.print(sumA1 >> 4);
        Serial.print(" ");
      }
      Serial.println();
  }
  while(true);
}

 
