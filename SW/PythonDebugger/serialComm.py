#!/usr/bin/python
import serial, time, sys, os, re, subprocess


class serialComm:

    def __init__(self):
        systems = {"darwin":"/dev/usb.","linux":"/dev/ttyACM"}
        system = re.match(r"([a-zA-Z]+)",sys.platform).group(0)
        print(subprocess.getoutput("ls "+systems[system]+"*"))
        port = input("Port: "+systems[system])
        self.ser = serial.Serial(
            port=systems[system]+port,
            baudrate=115200,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS
        )
        self.ser.isOpen()

    def getReadings(self):
        out = self.ser.readline().decode('ascii')
        return out.split(' ')

    def getDiodes(self):
        diodes = [0 for _ in range(16)]

        vals = []
        while(len(vals)<22):
            vals = self.getReadings()[1:]

        for i in range(0,22,3):
            diodes[int(vals[i])] = int(vals[i+1])
            diodes[int(vals[i])+8] = int(vals[i+2])
        return diodes

    def getDataSet(self,n):
        dataSet = []
        for i in range(n):
            d = self.getDiodes()
            print(i, d)
            dataSet.append(d)
        return dataSet


if __name__ == '__main__':
    s = serialComm()
    while 1:
        print(s.getDiodes())
