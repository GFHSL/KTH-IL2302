#!/usr/bin/python
import csv,sys
from serialComm import serialComm
import numpy as np
import matplotlib.pyplot as plt
if(sys.argv[1]=='-f' and sys.argv[2]):
    data = []
    with open(sys.argv[2], 'r') as csvfile:
        decoded = csv.reader(csvfile, delimiter=';')
        for row in decoded:
            data.append([int(x) for x in row])
else:
    n = int(sys.argv[1])
    s = serialComm()
    data = s.getDataSet(n)
dataset = []
adaa = np.array(data)
adaa = adaa.transpose()
dataset = adaa[0]
print(len(dataset))
#dataSet = np.array(data)[4]
print(np.std(data))
for i in range(0,len(adaa)):
    plt.figure(i)
    n, bins, patches = plt.hist(adaa[i],bins=50,range=(0,1023))
    plt.xlabel('A/D value')
    plt.ylabel('Count')
    plt.title('Diode ' + str(i))
    plt.savefig('./data/diode'+str(i)+'.png')
