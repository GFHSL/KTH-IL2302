#!/usr/bin/python
from serialComm import serialComm
import os,datetime,sys
File = os.path.dirname(os.path.realpath(__file__))+"/data/"+datetime.datetime.now().isoformat()+".csv"

s = serialComm()
n = int(sys.argv[1])
dataSet = s.getDataSet(n)
with open(File,'w') as f:
    for i in range(len(dataSet)):
        f.write(",".join(str(x) for x in dataSet[i])+"\n")
